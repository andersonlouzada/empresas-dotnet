﻿using System.Threading.Tasks;

namespace IMDb.Core.Data
{
  public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}