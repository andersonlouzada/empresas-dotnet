﻿using IMDb.Identidade.API.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDb.Identidade.API.Data
{
  public class IdentidadeRepository : IIdentidadeRepository
  {
    private readonly SignInManager<IdentityUser> _signInManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<IdentityUser> _userManager;
    public IdentidadeRepository(SignInManager<IdentityUser> signInManager,
                                RoleManager<IdentityRole> roleManager,
                                UserManager<IdentityUser> userManager)
    {
      _signInManager = signInManager;
      _roleManager = roleManager;
      _userManager = userManager;
      _signInManager = signInManager;
    }

    public void AdicionarRole(RoleViewModel roleViewModel)
    {
      throw new NotImplementedException();
    }

    public async Task<UsuarioRespostaLogin> CadastrarUser(UsuarioRegistro usuarioRegistro)
    {
      var user = new IdentityUser
      {
        UserName = usuarioRegistro.Nome,
        NormalizedUserName = usuarioRegistro.Nome,
        Email = usuarioRegistro.Email,
        EmailConfirmed = true
      };

      var result = await _userManager.CreateAsync(user, usuarioRegistro.Senha);

      if (result.Succeeded)
      {
        var role = new IdentityRole
        {
          Name = "USUARIO"
        };

        await _roleManager.CreateAsync(role);
        var roleResult = await _userManager.AddToRoleAsync(user, role.Name);

      }

      var erroResult = new UsuarioRespostaLogin();
      foreach(var error in result.Errors)
      {
        erroResult.ResponseResult.Errors.Mensagens.Add(error.Description);
      }

      return erroResult;
    }

    public async Task<UsuarioRespostaLogin> Login(UsuarioLogin usuarioLogin)
    {
      var result = await _signInManager.PasswordSignInAsync(usuarioLogin.Email, usuarioLogin.Senha,
          false, true);

      var erroResult = new UsuarioRespostaLogin();

      if (result.IsLockedOut)
          erroResult.ResponseResult.Errors.Mensagens.Add("Usuário temporariamente bloqueado por tentativas inválidas");

      erroResult.ResponseResult.Errors.Mensagens.Add("Usuário ou Senha incorretos");

      return erroResult;
    }

    public Task<IdentityUser> DesativarUser(string id)
    {
      throw new NotImplementedException();
    }

    public Task<RoleViewModel> EditarRole(string id)
    {
      throw new NotImplementedException();
    }

    public Task<IdentityUser> EditarUser(string id)
    {
      throw new NotImplementedException();
    }

    public Task<List<IdentityUser>> ListaUsersAtivos()
    {
      throw new NotImplementedException();
    }

    public Task<PagedResult<IdentityUser>> ListaUsersAtivosPaginado(int pageSize, int pageIndex, string query)
    {
      throw new NotImplementedException();
    }

  }
}
