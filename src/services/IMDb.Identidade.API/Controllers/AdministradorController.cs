﻿using IMDb.Identidade.API.Models;
using IMDb.WebAPI.Core.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Identidade.API.Controllers
{
  [Authorize(Roles = "ADMINISTRADOR,USUARIO")]
  [Route("api/[controller]")]
  public class AdministradorController : MainController
  {
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly UserManager<IdentityUser> _userManager;

    public AdministradorController(RoleManager<IdentityRole> roleManager,
        UserManager<IdentityUser> userManager)
    {
      _roleManager = roleManager;
      _userManager = userManager;
    }

    [Authorize(Roles = "ADMINISTRADOR")]
    [HttpGet("api/[controller]/lista-users-ativos")]
    public async Task<IActionResult> ListaUsersAtivos()
    {
      var role = _roleManager.Roles.FirstOrDefault(r => r.Name.Equals("USUARIO"));

      if (role == null)
      {
        AdicionarErroProcessamento("Função USUARIO não localizado");
        return CustomResponse();
      }

      var usersRole = new List<IdentityUser>();

      var users = _userManager.Users
          .Where(u => u.LockoutEnabled.Equals(true) && u.LockoutEnd.HasValue.Equals(false))
          .OrderBy(u => u.UserName).ToList();

      if (users == null)
      {
        AdicionarErroProcessamento("Nenhum usuário associado a uma função");
        return CustomResponse();
      }

      foreach (var u in users)
      {
        if (await _userManager.IsInRoleAsync(u, role.Name))
        {
          usersRole.Add(u);
        }
      }
      return CustomResponse(usersRole);
    }

    [Authorize(Roles = "ADMINISTRADOR")]
    [HttpGet("api/[controller]/lista-users-ativos-paginado")]
    public async Task<IActionResult> ListaUsersAtivosPaginado([FromQuery] int pageSize = 10, [FromQuery] int pageIndex = 1, [FromQuery] string query = "")
    {
      var role = _roleManager.Roles.FirstOrDefault(r => r.Name.Equals("USUARIO"));

      if (role == null)
      {
        AdicionarErroProcessamento("Função USUARIO não localizado");
        return CustomResponse();
      }

      var usuarios = _userManager.Users
          .Where(u => u.LockoutEnabled.Equals(true) && u.LockoutEnd.HasValue.Equals(false))
          .OrderBy(o => o.UserName)
          .Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();

      if (usuarios == null)
      {
        AdicionarErroProcessamento("Nenhum usuário associado a uma função");
        return CustomResponse();
      }

      var usersRole = new List<IdentityUser>();

      foreach (var u in usuarios)
      {
        if (await _userManager.IsInRoleAsync(u, role.Name))
        {
          usersRole.Add(u);
        }
      }

      return CustomResponse(new PagedResult<IdentityUser>
      {
        List = usersRole,
        TotalResults = usersRole.Count(),
        PageIndex = pageIndex,
        PageSize = pageSize,
        Query = query
      });

    }
  }
}
