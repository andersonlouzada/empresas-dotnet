﻿using System;
using System.Linq;

namespace IMDb.Identidade.API.Models
{
  public enum Funcoes
  {
    ADMINISTRADOR = 5,
    USUARIO = 4
  }

  public static class Funcao
  {
    public static int ObterEnumIdPeloNome(string nome)
    {
      var funcao = (Funcoes)Enum.Parse(typeof(Funcoes), nome);
      return (int)funcao;
    }

    public static string ObterEnumNomePeloId(int codigo)
    {
      var funcao = (Funcoes)codigo;
      return funcao.ToString();
    }

    public static bool ValidarNome(string nome)
    {
      bool eValido = false;
      foreach (var valor in Enum.GetNames(typeof(Funcoes)))
      {
        if (valor == nome)
        {
          eValido = true;
          return eValido;
        }
      }
      return eValido;
    }

    public static bool ValidarCodigo(int codigo)
    {
      bool eValido = false;
      foreach (var valor in Enum.GetValues(typeof(Funcoes)).Cast<Funcoes>())
      {
        if (valor.Equals(codigo))
        {
          eValido = true;
          return eValido;
        }
      }
      return eValido;
    }

    public static object RetornaEnumPeloNome(string nome)
    {
      foreach (var valor in Enum.GetNames(typeof(Funcoes)))
      {
        if (valor == nome)
        {
          return Enum.Parse(typeof(Funcoes), valor);
        }
      }
      return null;
    }
  } 
}
