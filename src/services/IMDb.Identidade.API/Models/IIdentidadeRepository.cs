﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDb.Identidade.API.Models
{
  public interface IIdentidadeRepository
  {
    Task<List<IdentityUser>> ListaUsersAtivos();
    Task<PagedResult<IdentityUser>> ListaUsersAtivosPaginado(int pageSize, int pageIndex, string query);
    void AdicionarRole(RoleViewModel roleViewModel);
    Task<UsuarioRespostaLogin> CadastrarUser(UsuarioRegistro usuarioRegistro);
    Task<UsuarioRespostaLogin> Login(UsuarioLogin usuarioLogin);
    Task<RoleViewModel> EditarRole(string id);
    Task<IdentityUser> EditarUser(string id);
    Task<IdentityUser> DesativarUser(string id);
  }
}
