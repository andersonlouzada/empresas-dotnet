﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace IMDb.Identidade.API.Configuration
{
  public static class SwaggerConfig
  {

    public static void AddSwaggerConfiguration(this IServiceCollection services)
    {
      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo()
        {
          Title = "IMDb Usuários (CRUD) API",
          Description = "Esta API faz parte do Desafio Pessoa Desenvolvedora .NET IMDb Site.",
          Contact = new OpenApiContact() { Name = "Anderson Luiz Louzada", Email = "valuz.anderson.to@gmail.com" },
          License = new OpenApiLicense() { Name = "MIT", Url = new Uri("https://bitbucket.org/ioasys/empresas-dotnet/") },
          Version = "v1"
        });

        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
          Description = "Insira o token JWT desta maneira: Bearer {seu token}",
          Name = "Authorization",
          Scheme = "Bearer",
          BearerFormat = "JWT",
          In = ParameterLocation.Header,
          Type = SecuritySchemeType.ApiKey
        });

        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
                {
                  new OpenApiSecurityScheme
                  {
                    Reference = new OpenApiReference
                    {
                      Type = ReferenceType.SecurityScheme,
                      Id = "Bearer"
                    }
                  },
                  new string[] { }
                }
        });

      });

    }

    public static void UseSwaggerConfiguration(this IApplicationBuilder app)
    {
      app.UseSwagger();
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
      });
    }

    //--------------------------------
    //public static IServiceCollection AddSwaggerConfiguration(this IServiceCollection services)
    //{
    //    services.AddSwaggerGen(c =>
    //    {
    //        c.SwaggerDoc("v1", new OpenApiInfo()
    //        {
    //            Title = "IMDb Identidade API",
    //            Description = "Esta API faz parte do Desafio Pessoa Desenvolvedora .NET IMDb Site.",
    //            Contact = new OpenApiContact() { Name = "Anderson Luiz Louzada", Email = "valuz.anderson.to@gmail.com" },
    //            License = new OpenApiLicense() { Name = "MIT", Url = new Uri("https://bitbucket.org/ioasys/empresas-dotnet/") },
    //            Version = "v1"
    //        });
    //    });

    //    return services;
    //}

    //public static IApplicationBuilder UseSwaggerConfiguration(this IApplicationBuilder app)
    //{
    //    app.UseSwagger();
    //    app.UseSwaggerUI(c =>
    //    {
    //        c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    //    });

    //    return app;
    //}
  }
}